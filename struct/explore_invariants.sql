-- do teams have multiple names per season? yep
select group_concat(name) from main.teamName
group by team_id, season
having count(name) > 1;

-- do participants have unique role per lifetime? nope
select actor_id, count(CASE WHEN is_coach is true THEN true END) as 'is_coach',
       count(CASE WHEN is_referee is true THEN true END) as 'is_referee',
       count(CASE WHEN is_coach is true THEN true END) as 'is_coach'
from main.participant
group by actor_id
order by actor_id;

-- do participants have unique role per match? nope
select actor_id, (case when is_coach is true then 1 else 0 END) +
    (case when is_referee is true then 1 else 0 END) +
    (case when is_starter is true then 1 else 0 END) as 'roles'
from main.participant
where roles > 1
order by actor_id;

-- are display names different than last name + first name? nope
select *
from main.participant
where first_name is not null and (instr(display_name, first_name) < 1 or instr(display_name, last_name < 1))
order by actor_id;

-- are actor.display_name and participant.display_name different? of course!
select actor.display_name from actor
left join participant p on actor.acbid = p.actor_id
where actor.display_name != p.display_name;