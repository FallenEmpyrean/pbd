set global sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

# all decisions were made considering the results of explore_invariants.sql
# and simple select distinct queries

use main;
DROP TABLE IF EXISTS team;
# TEAM
# dropped PK for acbid
# sqlgen:
# select acbid, founded_year from team;
create table team
(
    acbid        VARCHAR(5) not null
        primary key,
    founded_year INTEGER
);


create index team_acbid_idx
    on team (acbid);

DROP TABLE IF EXISTS teamName;
# TEAM_NAME
# dropped PK for (name, season), encorced unique name per season
# sqlgen:
# select distinct acbid, name, season  from teamName
# left join team t on teamName.team_id = t.id
# group by acbid, season;
create table teamName
(
    team_acbid VARCHAR(5)
        references team,
    name       VARCHAR(100) not null,
    season     INTEGER      not null,
    primary key (name, season)
);

create index teamName_team_id_idx
    on teamName (team_acbid);

DROP TABLE IF EXISTS actor;
# ACTOR
# dropped PK for acbid, dropped is_coach for the one in participant
# sqlgen:
# select acbid, display_name, full_name, nationality, birthplace, birthdate,
# position, height, weight, license, debut_acb, twitter from actor;
create table actor
(
    acbid        VARCHAR(5) primary key not null,
    display_name VARCHAR(50),
    full_name    varchar(50),
    nationality  TEXT,
    birthplace   TEXT,
    birthdate    DATETIME,
    position     TEXT,
    height       REAL                          null,
    weight       REAL                          null,
    license      TEXT,
    debut_acb    DATETIME,
    twitter      TEXT
);


create index actor_acbd_idx
    on actor (acbid);

create index actor_display_name_idx
    on actor (display_name);

DROP TABLE IF EXISTS game;
# GAME
# dropped score home, away extra and db_flag - always null
# sqlgen:
# select id, acbid, team_home_acbid, team_away_acbid, competition_phase, round_phase,
# journey, venue, attendance, kickoff_time, score_home, score_away, score_home_first,
# score_away_first, score_home_second, score_away_second, score_home_third, score_away_third,
# score_home_fourth, score_away_fourth  from game;
create table game
(
    acbid             VARCHAR(5) primary key not null,
    team_home_acbid   VARCHAR(5)
        references team,
    team_away_acbid   VARCHAR(5)
        references team,
    competition_phase TEXT,
    round_phase       TEXT,
    journey           INTEGER,
    venue             TEXT,
    attendance        INTEGER,
    kickoff_time      DATETIME,
    score_home        INTEGER,
    score_away        INTEGER,
    score_home_first  INTEGER,
    score_away_first  INTEGER,
    score_home_second INTEGER,
    score_away_second INTEGER,
    score_home_third  INTEGER,
    score_away_third  INTEGER,
    score_home_fourth INTEGER,
    score_away_fourth INTEGER
);

create index game_acbid_idx
    on game (acbid);

create index game_kickoff_time_idx
    on game (kickoff_time);

create index game_team_away_id_idx
    on game (team_away_acbid);

create index game_team_home_id_idx
    on game (team_home_acbid);

DROP TABLE IF EXISTS participant;
# PARTICIPANT
# dropped first name and last name as they are inconsitent and it can be reconstructed
# from the display name
# sqlgen:
# select participant.id, game_id, t.acbid as team_acbid, a.acbid as actor_acbid, participant.display_name, number,
#        participant.is_coach, is_referee, is_starter, minutes, point, t2_attempt, t2, t3_attempt, t3, t1_attempt,
#        t1, defensive_reb, offensive_reb, assist, steal, turnover, counterattack, block, received_block, dunk, fault,
#        received_fault, plus_minus, efficiency from participant
# left join team t on participant.team_id = t.id
# left join actor a on participant.actor_id = a.id;
create table participant
(
    id             INTEGER
        primary key,
    game_acbid     VARCHAR(5) not null references game,
    team_acbid     VARCHAR(5) references team,
    actor_acbid    VARCHAR(5) not null references actor,
    display_name   VARCHAR(50),
    number         INTEGER,
    is_coach       BOOLEAN,
    is_referee     BOOLEAN,
    is_starter     BOOLEAN,
    minutes        INTEGER,
    point          INTEGER,
    t2_attempt     INTEGER,
    t2             INTEGER,
    t3_attempt     INTEGER,
    t3             INTEGER,
    t1_attempt     INTEGER,
    t1             INTEGER,
    defensive_reb  INTEGER,
    offensive_reb  INTEGER,
    assist         INTEGER,
    steal          INTEGER,
    turnover       INTEGER,
    counterattack  INTEGER,
    block          INTEGER,
    received_block INTEGER,
    dunk           INTEGER,
    fault          INTEGER,
    received_fault INTEGER,
    plus_minus     INTEGER,
    efficiency     INTEGER
);



create index participant_actor_id_idx
    on participant (actor_acbid);

create index participant_game_id_idx
    on participant (game_acbid);

create index participant_team_id_idx
    on participant (team_acbid);

