rm reports/tmp/*

./console.sh < $1 > reports/tmp/res.txt

echo "$(cat reports/tmp/res.txt | head -n 1)" |
sed -e "s/\t/\n/g" |
while IFS= read -r column; do
  echo "<th class=\"cell100 columnh\">$column</th>" >> reports/tmp/header.html
done

echo "$(cat reports/tmp/res.txt | tail -n +2)" |
while IFS= read -r row; do
  echo "<tr class=\"row100 body\">" >> reports/tmp/body.html

  echo "$row" |
  sed -e "s/\t/\n/g" |
  while IFS= read -r column; do
    echo "<td class=\"cell100 columnh\">$column</td>" >> reports/tmp/body.html
  done

  echo "</tr>" >> reports/tmp/body.html
done


filename=$(echo $1 | sed -e "s/.sql/.html/g")
cp reports/template/template.html "reports/$filename"
sed -i '/{{header}}/r reports/tmp/header.html' "reports/$filename"
sed -i '/{{body}}/r reports/tmp/body.html' "reports/$filename"
sed -i 's/{{header}}//g' "reports/$filename"
sed -i 's/{{body}}//g' "reports/$filename"













