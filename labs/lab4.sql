# amintiri din anul 2
# 1. SELECT names, interogarile:
# 2,
SELECT name FROM world
  WHERE name LIKE '%y'
# 4,
SELECT name FROM world
  WHERE name LIKE '%land'
# 6,
SELECT name FROM world
  WHERE name LIKE '%oo%'
# 7.
SELECT name FROM world
  WHERE name LIKE '%a%a%a%'

# 2. SELECT from WORLD, interogarile:
# 3,
SELECT name FROM world
 WHERE population > 64105700
# 5,
SELECT name,population FROM world
 WHERE name in ('France','Germany','Italy')
# 7,
select name, population, area from world
  where population > 250000000 or area > 3000000
# 8,
select name, population, area from world
  where population > 250000000 xor area > 3000000
# 12.
SELECT name, capital
  FROM world
 WHERE left(name, 1) = left(capital, 1) and name <> capital

# 3. SELECT from NOBEL, interogarile:
# 4,
SELECT winner
FROM nobel
WHERE subject = 'Peace' AND yr >= 2000
# 5,
SELECT yr, subject, winner
FROM nobel
WHERE (yr >=1980 AND yr <=1989) AND subject = 'Literature'
# 6.
SELECT * FROM nobel
 WHERE winner in ('Theodore Roosevelt','Woodrow Wilson','Jimmy Carter','Barack Obama');

# 4. SELECT within SELECT, interogarile:
# 2,
SELECT name
FROM world
WHERE (gdp / population) > (SELECT gdp / population FROM world WHERE name = 'United Kingdom') AND continent = 'Europe';
# 4,
SELECT name, population
FROM world
WHERE population > (SELECT population FROM world WHERE name = 'Canada')
AND population < (SELECT population FROM world WHERE name = 'Poland');
# 7,
SELECT continent, name, area FROM world x
  WHERE area >= ALL
    (SELECT area FROM world y
        WHERE y.continent=x.continent
          AND area > 0)
# 9,
select name, continent, population from world
where continent not in (
select distinct continent from world where population > 25000000)
# 10.
select name, continent from world current
where population > all (
select population * 3 from world
where continent = current.continent and name <> current.name)
# 5. SUM and COUNT, interogarile:
# 2,
select distinct continent from world;
# 3,
select sum(gdp) from world where continent = 'Africa';
# 4,
select count(*) from world where area > 1000000;
# 6,
select continent, count(name) from world group by continent
# 7,
select continent, count(population) from world
where population > 10000000
group by continent
# 8.
select continent from world
group by continent
having sum(population) > 100000000
# 6. JOIN, interogarile:
# laughs in spanish unicode PTSD flashbacks from lab 3
# 4,
select team1, team2, player from goal
join game on id = matchid
where player like 'Mario%'
# 5,
SELECT player, teamid, coach, gtime
  FROM goal join eteam on teamid = id
 WHERE gtime<=10
# 6,
select mdate, teamname from game JOIN eteam ON (team1=eteam.id)
where coach = 'Fernando Santos';
# 7.
select player from goal
join game on matchid = id
where stadium = 'National Stadium, Warsaw'
# 7. More JOIN Operations, interogarile:
# 3,
SELECT id, title, yr
 FROM movie where title like '%Star Trek%'
 order by yr
# 6,
select name from actor join casting on actorid = id
where movieid = 11768;
# 12,
SELECT title, name FROM casting
join actor on actor.id = casting.actorid
join movie on movie.id = casting.movieid
where ord = 1 and movieid in (
select movieid from casting
join actor on actor.id = casting.actorid
where actor.name = 'Julie Andrews'
)
# 15.
# sqlzoo nu a fost fericit, dar nu am gasit diferente in rezultate
with common_movies as(
SELECT movieid FROM casting
join actor on actor.id = casting.actorid
join movie on movie.id = casting.movieid
where name = 'Art Garfunkel'
)
SELECT name FROM casting
join actor on actor.id = casting.actorid
where movieid in (select * from common_movies)
# 8. Using NULL, interogarile:
# 5,
select name, COALESCE(mobile, '07986 444 2266') from teacher
# 8,
# I do not abide by your rules. I like going left.
select dept.name, count(teacher.name) as staff from dept
left join teacher on dept.id = teacher.dept
group by dept.name;

# Feedback session:
# tot ce pot sa spun ca am invatat de pe sqlzoo a fost COALESCE #mereu imi place cand fac lucruri fara scop
