# INSERT
# a. Introduceţi cel puţin doi actori noi in baza de date utilizand
# instructiunea INSERT.

INSERT INTO main.actor (acbid, display_name, full_name, nationality, birthplace, birthdate, position, height, weight,
                        license, debut_acb, twitter)
VALUES ('kk001', 'Nielsen, Mat 1', 'Matthew Peter Nielsen 1', 'AUS', 'Penrith', '1978-02-03 00:00:00', 'F', 2.08, 0, '',
        '0000-00-00 00:00:00', '');
INSERT INTO main.actor (acbid, display_name, full_name, nationality, birthplace, birthdate, position, height, weight,
                        license, debut_acb, twitter)
VALUES ('kk002', 'Collins, DeJuan 1', 'DeJuan Lamont Collins 1', 'USA', 'Youngstown, Ohio', '1976-11-20 00:00:00', 'B',
        1.88, 86, '', '0000-00-00 00:00:00', '');


# b. Introduceţi înregistrari corespunzatoare in tabela Participant, asociind
# jucatorii cu echipe diferite si cu meciuri desfasurate dupa data de 1
# ianuarie 2017.

select *
from participant
where team_acbid in (select team_home_acbid
                     from game
                     where year(kickoff_time) > 2016
)
limit 2;

INSERT INTO main.participant (id, game_acbid, team_acbid, actor_acbid, display_name, number, is_coach, is_referee,
                              is_starter, minutes, point, t2_attempt, t2, t3_attempt, t3, t1_attempt, t1, defensive_reb,
                              offensive_reb, assist, steal, turnover, counterattack, block, received_block, dunk, fault,
                              received_fault, plus_minus, efficiency)
VALUES (1, '1', 'CLA', '519', 'McCalebb, Bo', 7, 0, 0, 1, 1326, 5, 5, 1, 1, 1, 0, 0, 3, 2, 3, 1, 3, 0, 0, 1, 0, 2, 0,
        -6, 5);
INSERT INTO main.participant (id, game_acbid, team_acbid, actor_acbid, display_name, number, is_coach, is_referee,
                              is_starter, minutes, point, t2_attempt, t2, t3_attempt, t3, t1_attempt, t1, defensive_reb,
                              offensive_reb, assist, steal, turnover, counterattack, block, received_block, dunk, fault,
                              received_fault, plus_minus, efficiency)
VALUES (2, '1', 'CLA', 'ADO', 'Casimiro, Luis', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0);

#  UPDATE
# a. Jucatorul Pablo Aguilar Bermudez devine atacant (A).

UPDATE actor
SET position     = 'A'
WHERE full_name = 'Pablo Aguilar Bermudez';

# b. Jucatorul Albert Oliver Campos devine anteror al echipei
# MORABANC ANDORRA.


select acbid, full_name from actor
where full_name like '%Oliver%';

UPDATE participant
SET is_coach = 1
WHERE actor_acbid = 'A56';

# c. Anul de apariţie al echipei avand, in sezonul 2016, numele
# MOVISTAR ETUDIANTES este de fapt 1950, nu 1948. Corectaţi
# eroarea.

select * from teamName
where name like '%NTES%' # When is language globalization coming? At this point I don't even care which one gets adopted.
and season = 2016;

UPDATE team
SET founded_year = 1950
WHERE acbid = 'EST';

# d. Toţi jucatorii mai in varsta de 35 de ani devin si arbitri.

with fossils as (
    select acbid from actor
    where year(from_days(datediff(now(), birthdate))) > 34
)
update participant
set is_coach = 1
where actor_acbid in (select acbid from fossils);


# DELETE
# a. Sa se stearga actorii care sunt mai in varsta decat 42 de ani.

with fossils as (
    select acbid from actor
    where year(from_days(datediff(now(), birthdate))) > 41
)
delete from actor
where acbid in (select acbid from fossils);

# b. Sa se stearga meciurile la care a participa actorul Albert Oliver.

with infected_games as (
    select distinct game_acbid from participant
    where actor_acbid = 'A56'
)
delete from game
where acbid in (select acbid from infected_games);


