# a. Trigger la inserare/modificare pe tabela Clienti (Clients):
# toti clientii inregistrati sa aiba varsta mai mare decat 18 ani.
delimiter //
create TRIGGER age_check
    BEFORE UPDATE
    ON customers
    FOR EACH ROW
BEGIN
    IF (NEW.age/*???? nu exista un camp legat de vartsa*/ < 18) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'age < 18';
    END IF;
END;
//
delimiter ;

# MySQL nu suporta multiple evenimente la trigger e.g. before insert, update, delete
delimiter //
create TRIGGER age_check
    BEFORE insert
    ON customers
    FOR EACH ROW
BEGIN
    IF (NEW.age/*???? nu exista un camp legat de vartsa*/ < 18) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'age < 18';
    END IF;
END;
//
delimiter ;

# b. Trigger la ştergere pe tabela Orders (Comenzi):
# să nu se poată şterge comenzile realizate în luna curentă.

delimiter //
create TRIGGER month_check
    BEFORE delete
    ON orders
    FOR EACH ROW
BEGIN
    IF (year(old.order_date) = year(now())) and month(old.order_date) = month((now())) THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'cannot delete orders from current month';
    END IF;
END;
//
delimiter ;

# c. Trigger la modificare pe tabela Angajați (Employees):
# să nu se poată modifica datele angajaților implicați în comenzi realizate în cursul anului curent.

delimiter //
create TRIGGER active_employee
    BEFORE update
    ON employees
    FOR EACH ROW
BEGIN
    IF count((select *
              from purchase_orders
              where created_by = old.id
                and year(submitted_date) = year(now()))) > 0 THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'cannot update employee with orders in current year';
    END IF;
END;
//
delimiter ;

# d. Trigger la inserare pe tabela Produse (Products):
# să nu se poată insera produse furnizate de către compania al carei director de marketing este Sato Naoki.

delimiter //
create TRIGGER ban_sato
    BEFORE insert
    ON products
    FOR EACH ROW
BEGIN
    IF count((select *
              from suppliers
              where last_name = 'Sato'
                and first_name = 'Naoki'
                and (old.supplier_ids like concat('%;', suppliers.id, '%') /* last in list */
                  or old.supplier_ids like concat('%', suppliers.id, ';%')) /* start or middle list */
    )) > 0 THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'Sato Naoki is banned';
    END IF;
END;
//
delimiter ;

# e. Trigger la stergere pe tabela Tranzacții (Inventory Transactions):
# o tranzacție odata ştearsă să fie trecută în tabela History_Transactions, având aceeaşi structură cu tabela initiala.

delimiter //
create TRIGGER archive_transation
    after delete
    ON inventory_transactions
    FOR EACH ROW
BEGIN

    INSERT INTO northwind.history_transactions (transaction_type, transaction_created_date,
                                                  transaction_modified_date, product_id, quantity, purchase_order_id,
                                                  customer_order_id, comments)
    VALUES (old.transaction_type, old.transaction_created_date, old.transaction_modified_date, old.product_id,
            old.quantity, old.purchase_order_id, old.customer_order_id, old.comments);
END;
//
delimiter ;


