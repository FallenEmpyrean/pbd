# d. Numele produselor cumpărate, numele clienților care au cumpărat produsul, țara clientului, precum şi valoarea totală a comenzilor pentru fiecare produs în parte.

select p.product_name, GROUP_CONCAT(CONCAT(c.last_name, ' ',  c.first_name, ' ', c.country_region)), sum(i.amount_due) as `total` from orders o
inner join order_details od on o.id = od.order_id
inner join products p on od.product_id = p.id
inner join customers c on o.customer_id = c.id
inner join invoices i on o.id = i.order_id
group by p.id;
