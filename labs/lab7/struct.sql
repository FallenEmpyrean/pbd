ALTER TABLE customers ADD type enum('own', 'prpr', 'pmgr', 'aa');

CREATE TABLE owner (
    `id` INT(11) NOT NULL,
    `income` real,
    CONSTRAINT `fk_owner`
    FOREIGN KEY (`id`)
    REFERENCES `northwind`.`customers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE purchasing_representative (
    `id` INT(11) NOT NULL,
    `experience` int(11),
    CONSTRAINT `fk_prpr`
    FOREIGN KEY (`id`)
    REFERENCES `northwind`.`customers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);


CREATE TABLE purchasing_manager (
    `id` INT(11) NOT NULL,
    `university` varchar(50),
    `master` varchar(50),
    `phd` varchar(50),
    CONSTRAINT `fk_pmgr`
    FOREIGN KEY (`id`)
    REFERENCES `northwind`.`customers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE TABLE accounting_assistant (
    `id` INT(11) NOT NULL,
    `studies` varchar(50),
    CONSTRAINT `fk_aa`
    FOREIGN KEY (`id`)
    REFERENCES `northwind`.`customers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);