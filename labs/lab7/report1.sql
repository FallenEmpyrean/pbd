# a. Denumirile produselor vandute, cantitatea totala vanduta din fiecare produs, in fiecare luna.

select p.product_name, sum(od.quantity), MONTHNAME(orders.paid_date) from orders
inner join order_details od on orders.id = od.order_id
inner join products p on od.product_id = p.id
group by month(orders.paid_date);