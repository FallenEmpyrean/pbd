# window func

select monthname(o.order_date) as month, FIRST_VALUE(p.product_name) OVER(PARTITION BY month(o.order_date) ORDER BY od.unit_price DESC) AS min_price_p_name
from orders o
inner join order_details od on o.id = od.order_id
inner join products p on od.product_id = p.id
where year(o.order_date) = 2006
group by month(o.order_date);