# c. Numele oraselor, numarul de comenzi pentru fiecare oras, inregistrate in cursul lunii curente.

select o.ship_city, count(distinct o.id) as `# of orders` from orders o
inner join order_details od on o.id = od.order_id
where month(paid_date) = month(now()) and year(paid_date) = year(now())
group by o.ship_city;