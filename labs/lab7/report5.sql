# e. Numele clientilor care au cumparat produsul cafea (Northwind Traders Coffee), orasul de provenienta al fiecarui client, cu grupare si ordonare dupa orase, apoi dupa numele clientilor

select c.last_name, c.first_name, c.city from orders o
inner join order_details od on o.id = od.order_id
inner join products p on od.product_id = p.id
inner join customers c on o.customer_id = c.id
where p.product_name = 'Northwind Traders Coffee'
group by c.city, c.last_name, c.first_name;