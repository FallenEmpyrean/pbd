# b. Numele clientilor (Customers), valoarea totala a comenzilor realizate, pentru fiecare client. Sa se afiseze si valoarea totala a tuturor comenzilor realizate, pentru toti clientii.

select c.last_name, c.first_name, sum(i.amount_due) as `total` from orders o
inner join order_details od on o.id = od.order_id
inner join products p on od.product_id = p.id
inner join customers c on o.customer_id = c.id
inner join invoices i on o.id = i.order_id
group by c.id;
