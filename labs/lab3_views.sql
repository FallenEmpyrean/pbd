# a. Numele echipelor corespunzatoare fiecarui sezon si anul aparitiei, pentru
# fiecare echipa.

drop view if exists team_names;
create view team_names as
select name, season, founded_year
from teamName
         left join team on team.acbid = teamName.team_acbid;

# b. Numele complet al jucatorului, nationalitatea si numele echipelor din care a
# facut parte. Daca actorul nu a facut parte din nicio echipa, sa se afiseze
# „fara echipa”

drop view if exists player_info;
create view player_info as
select full_name, nationality, (IF(count(name) > 0, group_concat(name), 'fara echipa')) as 'teams'
from participant
         left join actor a on participant.actor_acbid = a.acbid
         left join teamName tN on participant.team_acbid = tN.team_acbid
group by participant.actor_acbid;


# c. Numele echipei gazda, numele echipei oaspete, data si ora de inceput,
# locatia, scorul general, pentru toate finalele.
drop view if exists finals;
create view finals as
select ht.name as 'home team',
       at.name as 'away team',
       date(kickoff_time),
       hour(kickoff_time),
       venue,
       score_home,
       score_away
from game
         left join teamName ht on game.team_home_acbid = ht.team_acbid
         left join teamName at on game.team_away_acbid = at.team_acbid
where round_phase = 'final';


# d. Numele echipei, sezonul si numarul de meciuri castigate de fiecare echipa
# per sezon. Daca o anumita echipa nu a castigat niciun meci in sezonul
# respectiv, se va afisa valoarea 0.

drop view if exists seasonal_wins;
create view seasonal_wins as
select name, wins.season, count(acbid) as wins
from (select year(game.kickoff_time) as season, team_home_acbid as acbid
      from game
      where score_home > score_away
      union all
      select year(game.kickoff_time) as season, team_away_acbid as acbid
      from game
      where score_home < score_away) as wins
         left join teamName
                   on teamName.team_acbid = acbid
group by acbid, season;

# e. Numele echipelor si numarul de meciuri castigate de fiecare echipa, ca
# echipa oaspete.

drop view if exists home_wins;
create view home_wins as
select name, count(acbid) as wins
from (select year(game.kickoff_time) as season, team_home_acbid as acbid
      from game
      where score_home > score_away) as wins
         left join teamName
                   on teamName.team_acbid = acbid
group by acbid;


# f. Alte vederi pe care le considerati utile.
# I have already seen more than I wanted to. #200k lines of unicode