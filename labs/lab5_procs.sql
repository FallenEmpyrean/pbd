# 2. Să se creeze, în aceeaşi bază de date, următoarele proceduri stocate:

# a. Procedură stocată pentru a afişa numele întreg, naționalitatea, data şi locul
# naşterii, pentru jucătorii din echipa al cărei nume este dată ca parametru.

DELIMITER //
DROP PROCEDURE IF EXISTS players_of_team //
CREATE PROCEDURE
    players_of_team(team_name varchar(50))
BEGIN
    select distinct full_name, nationality, birthdate, birthplace
    from teamName
             left join participant p on teamName.team_acbid = p.team_acbid
             left join game g on p.game_acbid = g.acbid
             left join actor a on p.actor_acbid = a.acbid
    where teamName.name = team_name;
END
//

call players_of_team('LUCENTUM ALICANTE');

# b. Procedură stocată pentru afişarea numărului de meciuri jucate de către
# jucătorul al cărui nume este dat ca parametru.

DELIMITER //
DROP PROCEDURE IF EXISTS count_games_of_player //
CREATE PROCEDURE
    count_games_of_player(player_name varchar(50))
BEGIN
    select count(distinct g.acbid)
    from actor
             left join participant p on actor.acbid = p.actor_acbid
             left join game g on p.game_acbid = g.acbid
    where actor.full_name = player_name;
END
//

call count_games_of_player('Amal Omari McCaskill');

# c. Procedură stocată pentru inserarea unui participant, cu următorii parametri:
# acbid-ul meciului, numele echipei, numele complet al actorului, numărul,
# numărul de minute jucate, numărul de puncte, etc.

DELIMITER //
DROP PROCEDURE IF EXISTS insert_participant //
CREATE PROCEDURE
    insert_participant(game_acbid VARCHAR(5),
                       team_name VARCHAR(50),
                       actor_name VARCHAR(50),
                       display_name VARCHAR(50),
                       number INTEGER,
                       is_coach BOOLEAN,
                       is_referee BOOLEAN,
                       is_starter BOOLEAN,
                       minutes INTEGER,
                       point INTEGER,
                       t2_attempt INTEGER,
                       t2 INTEGER,
                       t3_attempt INTEGER,
                       t3 INTEGER,
                       t1_attempt INTEGER,
                       t1 INTEGER,
                       defensive_reb INTEGER,
                       offensive_reb INTEGER,
                       assist INTEGER,
                       steal INTEGER,
                       turnover INTEGER,
                       counterattack INTEGER,
                       block INTEGER,
                       received_block INTEGER,
                       dunk INTEGER,
                       fault INTEGER,
                       received_fault INTEGER,
                       plus_minus INTEGER,
                       efficiency INTEGER)
BEGIN
    INSERT INTO main.participant
    (game_acbid, team_acbid, actor_acbid, display_name, number, is_coach, is_referee, is_starter, minutes, point,
     t2_attempt, t2, t3_attempt, t3, t1_attempt, t1, defensive_reb, offensive_reb, assist, steal, turnover,
     counterattack, block, received_block, dunk, fault, received_fault, plus_minus, efficiency)
    VALUES (game_acbid,
            (
                select teamName.team_acbid
                from teamName
                where teamName.name = team_name
                limit 1
            ),
            (
                select acbid
                from actor
                where actor.full_name = actor_name
                limit 1
            ),
            actor_name,
            number,
            is_coach,
            is_referee,
            is_starter,
            minutes,
            point,
            t2_attempt,
            t2,
            t3_attempt,
            t3,
            t1_attempt,
            t1,
            defensive_reb,
            offensive_reb,
            assist,
            steal,
            turnover,
            counterattack,
            block,
            received_block,
            dunk,
            fault,
            received_fault,
            plus_minus,
            efficiency);

END
//

#INSERT INTO main.participant VALUES (5, '61001', 'CLA', 'FQH', 'Pasecniks, Anzejs', 14, 0, 0, 0, 379, 2, 2, 1, 0, 0, 0, 0, 1, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, -6, 5);

call insert_participant('MORABANC ANDORRA', 'Amal Omari McCaskill', 'Pasecniks, Anzejs', 14, 0, 0, 0, 379, 2, 2, 1, 0,
                        0, 0, 0, 1, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, -6, 5);


# d. Procedură stocată pentru actualizarea numelui unei anumite echipe, atunci
# când sezonul, vechiul şi noul nume ale ecipei respective sunt date ca
# parametru.

DELIMITER //
DROP PROCEDURE IF EXISTS update_team_name //
CREATE PROCEDURE
    update_team_name(old_team_name varchar(50), new_team_name varchar(50), _season integer)
BEGIN
    update teamName set teamName.name = new_team_name
        where teamName.name = old_team_name and teamName.season = _season;
END
//

call update_team_name('MORABANC ANDORRA', 'new name', 2016);

# e. Procedură stocată pentru ştergerea unui anumit meci. Numele echipei
# gazdă, numele echipei oaspete, data şi ora începerii meciului
# (kickoff_time) sunt date ca parametri.

DELIMITER //
DROP PROCEDURE IF EXISTS delete_game_where //
CREATE PROCEDURE
    delete_game_where(home_team_name varchar(50), away_team_name varchar(50), _kickoff_time datetime)
BEGIN
    delete from game where game.kickoff_time = _kickoff_time
                       and game.team_home_acbid in (select acbid from teamName where teamName.name = home_team_name)
    and game.team_away_acbid in (select acbid from teamName where teamName.name = away_team_name);

END
//

select th.name, ta.name, kickoff_time from game
left join teamName ta on ta.team_acbid = game.team_away_acbid
left join teamName th on th.team_acbid = game.team_home_acbid
limit 5;

call delete_game_where('LECHE RÃO BREOGÃN', 'AXA F.C. BARCELONA', '1994-09-11 13:15:00');
