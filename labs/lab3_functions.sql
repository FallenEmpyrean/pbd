# Creaţi în baza de date urmatoarele functii Transact-SQL:
# a. Sa se creeze o functie care sa afiseze varsta in ani a actorului al carui nume
# (display name) este dat ca parametru ( a se lua in considerare ziua de
# nastere).

drop function if exists year_diff;
create function year_diff(d datetime) returns integer deterministic
    return year(from_days(datediff(now(), d)));

create function age_of(display_name varchar(50)) returns integer deterministic
    return (select year_diff(birthdate)
            from main.actor
            where actor.display_name = display_name
            limit 1);

select age_of('Gregory, Kenny');

# b. Sa se creeze o functie care sa afiseze numele jucatorilor mai tineri de 25 de
# ani din echipa a carei denumire este data ca parametru, pentru sezonul dat
# ca parametru.

DELIMITER //

DROP PROCEDURE IF EXISTS young_players //

CREATE PROCEDURE
    young_players(team_name varchar(50), playing_season integer)
BEGIN
    select distinct full_name, year_diff(a.birthdate) as age, teamName.name as 'team name'
    from teamName
             left join participant p on teamName.team_acbid = p.team_acbid
             left join game g on p.game_acbid = g.acbid
             left join actor a on p.actor_acbid = a.acbid
    where teamName.name = team_name
      and year(g.kickoff_time) = playing_season
      and year_diff(a.birthdate) < 25;
END
//

DELIMITER ;

select full_name, year_diff(a.birthdate), year(kickoff_time), teamName.name as age
from teamName
         left join participant p on teamName.team_acbid = p.team_acbid
         left join game g on p.game_acbid = g.acbid
         left join actor a on p.actor_acbid = a.acbid
where year_diff(a.birthdate) < 25;

call young_players('AXA F.C. BARCELONA', 2017);

# c. Sa se creeze o functie care sa afiseze numarul de meciuri castigate de
# echipa al carei acbid este dat ca parametru

create function won_games(_acbid varchar(5)) returns integer deterministic
    return (select name, count(acbid) as wins
            from (select year(game.kickoff_time) as season, team_home_acbid as acbid
                  from game
                  where score_home > score_away
                  union all
                  select year(game.kickoff_time) as season, team_away_acbid as acbid
                  from game
                  where score_home < score_away) as wins
                     left join teamName
                               on teamName.team_acbid = acbid
            where acbid = _acbid
            group by acbid
            limit 1);

select name, count(acbid) as wins
from (select year(game.kickoff_time) as season, team_home_acbid as acbid
      from game
      where score_home > score_away
      union all
      select year(game.kickoff_time) as season, team_away_acbid as acbid
      from game
      where score_home < score_away) as wins
         left join teamName
                   on teamName.team_acbid = acbid
group by acbid;