SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `maths`;
CREATE SCHEMA IF NOT EXISTS `maths`;
USE `maths`;


-- -----------------------------------------------------
-- Table `maths`.`persons`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `maths`.`persons`
(
    `id`           INT(11)      NOT NULL AUTO_INCREMENT,
    `name`         VARCHAR(100) NULL DEFAULT NULL,
    `supervisor_1` int(11)      NULL DEFAULT NULL,
    `supervisor_2` int(11)      NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_persons_supervisor_1`
        FOREIGN KEY (`supervisor_1`)
            REFERENCES `maths`.`persons` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,
    CONSTRAINT `fk_persons_supervisor_2`
        FOREIGN KEY (`supervisor_2`)
            REFERENCES `maths`.`persons` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
