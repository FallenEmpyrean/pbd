# a. Definiti o interogare care sa afiseze numarul de descendenti pentru fiecare nod.

with recursive cte (id, name, parent_id, ancestor_id, ancestor_name) as (
    select seed.id,
           seed.name,
           seed.parent_id,
           seed.id,
           seed.name
    from persons seed
    union all
    select p.id,
           p.name,
           p.parent_id,
           cte.ancestor_id,
           cte.ancestor_name
    from persons p
             inner join cte
                        on p.parent_id = cte.id
)
select ancestor_name, count(*)
from cte
where id != ancestor_id # parent is not descendant of itself
group by ancestor_id;

# b. Definiti o vedere care sa afiseze fiecare nod cu descendentii sai directi.

#drop view if exists direct_descendants;
#create view direct_descendants as
with recursive cte (id, name, parent_id, ancestor_id, ancestor_name) as (
    select seed.id,
           seed.name,
           seed.parent_id,
           seed.id,
           seed.name
    from persons seed
    union all
    select p.id,
           p.name,
           p.parent_id,
           cte.ancestor_id,
           cte.ancestor_name
    from persons p
             inner join cte
                        on p.parent_id = cte.id
)
select ancestor_name, group_concat(name) as 'children'
from cte
where parent_id = ancestor_id
  and parent_id IS NOT NULL # direct descentant only
group by ancestor_id;

# c. Definiti o procedura stocata care sa afiseze toti descendentii unui anumit nod, dat ca parametru.

DELIMITER //
DROP PROCEDURE IF EXISTS all_desc_of //
CREATE PROCEDURE
    all_desc_of(_id int(11))
BEGIN
    with recursive cte (id, name, parent_id, ancestor_id, ancestor_name) as (
        select seed.id,
               seed.name,
               seed.parent_id,
               seed.id,
               seed.name
        from persons seed
        union all
        select p.id,
               p.name,
               p.parent_id,
               cte.ancestor_id,
               cte.ancestor_name
        from persons p
                 inner join cte
                            on p.parent_id = cte.id
    )
    select name
    from cte
    where ancestor_id = _id
      and id != ancestor_id;
END
//

call all_desc_of(3);


# d. Definiti un trigger care sa nu permita stergerea unui anumit nod daca acesta are cel putin un descendent.

delimiter //
create TRIGGER has_desc
    before delete
    ON persons
    FOR EACH ROW
BEGIN
    if count((with recursive cte (id, name, parent_id, ancestor_id, ancestor_name) as (
        select seed.id,
               seed.name,
               seed.parent_id,
               seed.id,
               seed.name
        from persons seed
        union all
        select p.id,
               p.name,
               p.parent_id,
               cte.ancestor_id,
               cte.ancestor_name
        from persons p
                 inner join cte
                            on p.parent_id = cte.id
    )
              select name
              from cte
              where ancestor_id = old.id
                and id != ancestor_id)) > 0 then
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'This person has at least one descendant';
    end if;
END;
//
delimiter ;