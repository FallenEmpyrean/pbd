# a. Definiti o vedere care sa afiseze suprevizorii fiecarui matematician.

#drop view if exists supervisors;
#create view supervisors as
select p.name, CONCAT_WS(', ', s1.name, s2.name) as `supervisors`
from persons p
         left join persons s1 on s1.id = p.supervisor_1
         left join persons s2 on s2.id = p.supervisor_2;

# b. Definiti o procedura stocata care sa afiseze numarul de discipoli de sex masculin ai unui anumit nod si numarul de discipoli de sex feminin ai aceluiasi nod, dat ca parametru.
# Error 404, acest student nu a gasit sexul matematicienilor in aceasta baza de date, voi rezolva acelasi enunt, dar fara sexe

DELIMITER //
DROP PROCEDURE IF EXISTS all_desc_of //
CREATE PROCEDURE
    all_desc_of(_id int(11))
BEGIN
    with recursive cte (id, ancestor_id) as (
        select seed.id,
               seed.id
        from persons seed
        where seed.id = _id
        union all
        select p.id, cte.ancestor_id
        from persons p
                 inner join cte on p.supervisor_1 = cte.id
        union all
        select p.id, cte.ancestor_id
        from persons p
                 inner join cte on p.supervisor_2 = cte.id
    )
    select count(distinct id)
    from cte
    where id != ancestor_id;
END //

call all_desc_of(17);


# c. Definiti un trigger care sa nu permita stergerea unui anumit nod daca acesta are cel putin doi descendenti.

delimiter //
create TRIGGER has_desc
    before delete
    ON persons
    FOR EACH ROW
BEGIN
    if count((with recursive cte (id, ancestor_id) as (
        select seed.id,
               seed.id
        from persons seed
        where seed.id = old.id
        union all
        select p.id, cte.ancestor_id
        from persons p
                 inner join cte on p.supervisor_1 = cte.id
        union all
        select p.id, cte.ancestor_id
        from persons p
                 inner join cte on p.supervisor_2 = cte.id
    )
    select distinct id
    from cte
    where id != ancestor_id)) > 0 then
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'This person has at least one descendant';
    end if;
END;
//
delimiter ;