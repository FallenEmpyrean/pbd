set session sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

# Afişați primele zece nume distincte de echipe, din lista numelor de echipe ordonată alfabetic.

select distinct name from teamName
order by name
limit 10;


# Afişați numele complet, naționalitatea şi locul naşterii pentru toți antrenorii
# din baza de date

select full_name, nationality, birthplace from actor
where acbid in (
    select acbid from participant
    where is_coach is true
);


# Afişați numele distincte ale jucătorilor şi numele echipelor din care fac parte.
# Există jucători care fac parte din cel puțin două echipe?

select acbid, full_name, count(n.team_acbid) as team_number, group_concat(n.name) as team_names
from (select distinct actor.acbid, actor.full_name, p.team_acbid from actor
left join participant p on actor.acbid = p.actor_acbid) as player_teams
left join teamName n on player_teams.team_acbid = n.team_acbid
group by acbid;


# Afişați acbid-urile echipelor care au activat atât în sezonul 2014, cât şi în
# sezonul 2016.

select distinct team_acbid
from teamName
where team_acbid in (
    select teamName.team_acbid
    from teamName
    where season = 2014
)
  and team_acbid in (
    select teamName.team_acbid
    from teamName
    where season = 2016
);


# Afişați numărul meciurilor jucate în fiecare an.

select YEAR(kickoff_time) as year, count(*) as games from game
group by YEAR(kickoff_time);


# Găsiți numele jucătorilor şi numărul de meciuri la care a participat fiecare
# jucător. Listați şi jucătorii care nu au participat la niciun meci.

select display_name, count(*) as games from participant
where is_coach is false and is_referee is false
group by actor_acbid;


# Găsiți numele echipelor şi numărul de membri din fiecare echipă.

select tN.name, count(distinct actor_acbid) as player_number from participant
left join teamName tN on participant.team_acbid = tN.team_acbid
where is_referee = false and is_referee = false
group by name;
