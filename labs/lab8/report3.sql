# Un raport care sa afiseze numele produsului si valoarea totala a comenzilor pentru
# acel produs pe anii 1996, 1997 si 1998.

select year(o.order_date), p.product_name,
       FILTER(WHERE sales_pipeline.close_value > 1000)) from orders o
inner join order_details od on o.id = od.order_id
inner join products p on od.product_id = p.id
#where year(o.order_date) >= 1996 and year(o.order_date) <= 1998
group by p.id, year(o.order_date)

# MySQL nu pare sa aibe filter pentru a face selectia per coloane, combinat cu rezultatele din raportul #1
# acest query devine aproape imposibil