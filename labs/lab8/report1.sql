# Un raport de tip matriceal care sa afiseze pe linii numele produselor comandate,
# pe coloane numele companiilor care au comandat produsul respectiv,
# iar în celule cantitatea comandată din produsul respectiv. Să se afişeze,
# de asemenea, imaginea fiecărui produs într-o coloană separată.
# Să se poată specifica numele produsului ca parametru.

# din ce am cautat nu exista statement-ul de pivot in MySQL
# am gasit varianta asta https://riptutorial.com/mysql/example/10441/creating-a-pivot-query
# dar consider ca e prea work-around pentru practica

# fiind in contextul de MySQL pe Ubuntu la ultimul laborator, nu consider ca se merita sa cauta/creez o
# solutie non-Microsoft