# Un raport de tip tabular care sa afiseze denumirile si imaginile produselor comandate de clientii din orasul
# al carui nume este specificat ca parametru, numele si prenumele clientilor, valoarea totala a comenzii si anul,
# cu grupare dupa numele si prenumele clientului, apoi dupa an. Raportul sa includa subtotaluri si sa permita operatia
# drilldown.

# din aceleasi motive ca si in primul raport, lipsesc anumite componente

select p.product_name, GROUP_CONCAT(CONCAT(c.last_name, ' ', c.first_name, ', ')),
       sum(od.unit_price), year(o.order_date) from orders o
inner join order_details od on o.id = od.order_id
inner join products p on od.product_id = p.id
inner join customers c on o.customer_id = c.id
where c.city = 'Boston'
group by p.id;


DELIMITER //

DROP PROCEDURE IF EXISTS query_town //

CREATE PROCEDURE
    query_town(town_name varchar(50))
BEGIN
    select p.product_name, GROUP_CONCAT(CONCAT(c.last_name, ' ', c.first_name, ', ')),
           sum(od.unit_price), year(o.order_date) from orders o
    inner join order_details od on o.id = od.order_id
    inner join products p on od.product_id = p.id
    inner join customers c on o.customer_id = c.id
    where c.city = town_name
    group by p.id;
END
//

DELIMITER ;

call query_town('Boston');