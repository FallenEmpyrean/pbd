set session sql_mode = (SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));

# Numele şi vârsta celui mai tânăr actor din baza de date şi numărul de meciuri
# la care acesta a participat.

with youngest as (
    select acbid, full_name, birthdate, year(from_days(datediff(now(), birthdate))) as age
    from actor
    order by birthdate desc
    limit 1
)
select full_name, age, count(*) as 'games'
from participant
         inner join youngest on participant.actor_acbid = youngest.acbid
group by acbid;


# Numele echipei gazdă, numele echipei oaspete, precum şi locația desfăşurării,
# pentru meciurile jucate în cursul anului 2017.

with team_names as (
    select team_acbid as acbid, name
    from teamName
    group by team_acbid
)
select thn.name as 'home team', tan.name as 'away team', venue
from game
         left join team_names thn on game.team_home_acbid = thn.acbid
         left join team_names tan on game.team_away_acbid = tan.acbid
where year(kickoff_time) = 2017;


# Numele echipelor gazdă care au ajuns în finala competiției în ultimii trei ani

with team_names as (
    select team_acbid as acbid, name
    from teamName
    group by team_acbid
)
select distinct thn.name as 'home team'
from game
         left join team_names thn on game.team_home_acbid = thn.acbid
where year(from_days(datediff(now(), kickoff_time))) < 4
  and round_phase = 'final';


# Acb-id-urile şi anul apariției fiecărei echipe care conține jucători de cel puțin
# trei naționalități diferite.

select team_acbid, founded_year
from participant
         left join actor a on participant.actor_acbid = a.acbid
         left join team on team_acbid = team.acbid
group by team_acbid
having count(distinct nationality) > 2;

# Acb-id-urile acelor echipe care au câştigat toate meciurile la care au participat.

select acbid
from team
where acbid not in (
    select distinct team_home_acbid as acbid
    from game
    where score_home < score_away
    union
    select distinct team_away_acbid as acbid
    from game
    where score_home > score_away
);


# Alte 5 interogări pe care le considerați utile.
# See: struct/explore_invariants.sql