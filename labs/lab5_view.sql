# 1. Sa se creeze, in baza de date corespunzatoare Ligii Spaniole de Baschet,
# urmatoarele vederi:

# a. Numele întreg, naționalitatea, data şi locul naşterii, inălțimea, greutatea, poziția
# în joc, pentru fiecare jucător care a paricipat la cel puțin un meci în sezonul
# 2015.

drop view if exists player_info_2015;
create view player_info_2015 as
select full_name, nationality, birthdate, birthplace, height, weight, position
from participant
        inner join actor a on participant.actor_acbid = a.acbid
        left join game g on participant.game_acbid = g.acbid
        inner join teamName tN on participant.team_acbid = tN.team_acbid
where year(kickoff_time) = 2015
group by participant.actor_acbid;


# b. Numele echipei gazdă, numele echipei oaspete, locul desfăşurării meciului,
# faza competiției, scorul, numele şi naționalitatea arbitrului pentru toate
# meciurile desfăşurate în cursul anului 2016.

drop view if exists coaches_2016;
create view coaches_2016 as
select tH.name, tA.name, venue, competition_phase, score_home, score_away, a.full_name, nationality
from participant
        inner join actor a on participant.actor_acbid = a.acbid
        left join game g on participant.game_acbid = g.acbid
        left join teamName tH on tH.team_acbid = g.team_home_acbid
        left join teamName tA on tA.team_acbid = g.team_away_acbid
where year(kickoff_time) = 2016 and is_coach = 1
group by participant.actor_acbid;


# c. Numele fiecărei echipe, sezonul si numărul de naționalități distincte ale
# jucătorilor din acea echipă.

drop view if exists team_nationality_count;
create view team_nationality_count as
select tN.name, tN.season, count(distinct nationality)
from participant
        inner join actor a on participant.actor_acbid = a.acbid
        inner join teamName tN on tN.team_acbid = participant.team_acbid
group by participant.team_acbid;

# d. Modificați vederea de la punctul a. astfel încât să se afişeze datele
# corespunzătoare jucătorilor care au activat atât în sezonul 2015, cât şi în
# 2016.

drop view if exists player_info_2015_2016;
create view player_info_2015_2016 as
select full_name, nationality, birthdate, birthplace, height, weight, position
from participant
        inner join actor a on participant.actor_acbid = a.acbid
        left join game g on participant.game_acbid = g.acbid
        inner join teamName tN on participant.team_acbid = tN.team_acbid
where year(kickoff_time) = 2015 and a.acbid in (
    select p2.actor_acbid from participant p2
    left join game g2 on g2.acbid = p2.game_acbid
    where year(g2.kickoff_time) = 2016
    )
group by participant.actor_acbid;

# e. Modificați vederea de la punctul b. schimbând faza meciurilor
# desfăşurate în sferturile de finală, aşa încât acestea să apară în
# semifinalele competițiilor.

drop view if exists coaches_2016_semifinals;
create view coaches_2016_semifinals as
select tH.name, tA.name, venue, competition_phase, score_home, score_away, a.full_name, nationality
from participant
        inner join actor a on participant.actor_acbid = a.acbid
        left join game g on participant.game_acbid = g.acbid
        left join teamName tH on tH.team_acbid = g.team_home_acbid
        left join teamName tA on tA.team_acbid = g.team_away_acbid
where year(kickoff_time) = 2016 and is_coach = 1 and round_phase = 'semifinal'
group by participant.actor_acbid;