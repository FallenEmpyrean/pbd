# a. Afisati numele, prenumele si orasele clientilor care
# au realizat comenzi pentru produse din categoria al carei nume este specificat ca parametru.

DELIMITER //
DROP PROCEDURE IF EXISTS customers_by_p_cat //
CREATE PROCEDURE
    customers_by_p_cat(_category VARCHAR(50))
BEGIN
    select c.last_name, c.first_name, c.city
    from orders
             inner join customers c on orders.customer_id = c.id
             inner join order_details od on orders.id = od.order_id
             inner join products p on od.product_id = p.id
    where category = _category
    group by c.id;
END
//

call customers_by_p_cat('Condiments');

# b. Afisati numarul de clienti care au comandat produsul
# al carui nume este specificat ca parametru.

DELIMITER //
DROP PROCEDURE IF EXISTS customer_count_by_p_name //
CREATE PROCEDURE
    customer_count_by_p_name(_name VARCHAR(50))
BEGIN
    select count(distinct c.id)
    from orders
             inner join customers c on orders.customer_id = c.id
             inner join order_details od on orders.id = od.order_id
             inner join products p on od.product_id = p.id
    where p.product_name = _name;
END
//

call customer_count_by_p_name('Northwind Traders Cajun Seasoning');

# c. Creati o procedura de inserare a comenzilor, cu urmatorii parametri:
# nume si prenume client, nume si prenume angajat, nume produs, cantitate, valoare totala,
# data livrarii comenzii, numele transportorului, detalii legate de adresa de livrare, etc.

select id
from shippers
where last_name = 'Smith';

select id
from customers
where first_name = 'John'
  and last_name = 'Smith';

select id
from employees
where first_name = 'John'
  and last_name = 'Smith';

select id
from products
where product_name = 'Northwind Traders Cajun Seasoning';

INSERT INTO northwind.orders (id, employee_id, customer_id, order_date, shipped_date, shipper_id, ship_name,
                              ship_address, ship_city, ship_state_province, ship_zip_postal_code, ship_country_region,
                              shipping_fee, taxes, payment_type, paid_date, notes, tax_rate, tax_status_id, status_id)
VALUES (30, 9, 27, '2006-01-15 00:00:00', '2006-01-22 00:00:00', 2, 'Karen Toh', '789 27th Street', 'Las Vegas', 'NV',
        '99999', 'USA', 200.0000, 0.0000, 'Check', '2006-01-15 00:00:00', null, 0, null, 3);

INSERT INTO northwind.order_details (id, order_id, product_id, quantity, unit_price, discount, status_id,
                                     date_allocated, purchase_order_id, inventory_id)
VALUES (27, 30, 34, 100.0000, 14.0000, 0, 2, null, 96, 83);

# Nu am mai combinat acesta procedura enorm de lunga, se face celei din labul 5 cu select in values(...),
# dar cu doua inserturi si multa rabdare

# d. Creati o procedura de modificare a datelor personale pentru clienții al caror nume şi prenume
# sunt date ca parametri, noile valori pentru companie, adresa de email, telefon, fax, adresa fizica, etc.
# fiind de asemenea furnizate ca parametri.

DELIMITER //
DROP PROCEDURE IF EXISTS update_customer //
CREATE PROCEDURE
    update_customer(_company VARCHAR(50),
                    _last_name VARCHAR(50),
                    _first_name VARCHAR(50),
                    _email_address VARCHAR(50),
                    _job_title VARCHAR(50),
                    _business_phone VARCHAR(25),
                    _home_phone VARCHAR(25),
                    _mobile_phone VARCHAR(25),
                    _fax_number VARCHAR(25),
                    _address LONGTEXT,
                    _city VARCHAR(50),
                    _state_province VARCHAR(50),
                    _zip_postal_code VARCHAR(15),
                    _country_region VARCHAR(50),
                    _web_page LONGTEXT,
                    _notes LONGTEXT,
                    _attachments LONGBLOB)
BEGIN

    update northwind.customers
    set company         = _company,
        email_address   = _email_address,
        job_title       = _job_title,
        business_phone  = _business_phone,
        home_phone      = _home_phone,
        mobile_phone    = _mobile_phone,
        fax_number      = _fax_number,
        address         = _address,
        city            = _city,
        state_province  = _state_province,
        zip_postal_code = _zip_postal_code,
        country_region  = _country_region,
        web_page        = _web_page,
        notes           = _notes,
        attachments     = _attachments
    where first_name = _first_name
      and last_name = _last_name;
END
//

call update_customer('Bedecs', 'Anna', 'Company A', null, 'Owner', '(123)555-0100', null, null, '(123)555-0101',
                     '123 1st Street',
                     'Seattle', 'WA', '99999', 'USA', null, null, 0x);