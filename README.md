# Lab Report

## Project Structure

* **labs** containts the individual queries which are lab specific and have no outside impact
* **struct** contains creation statements and processed data, also queries which were used to analyse the data to remove redundacies(*explore_invariants.sql*)
* **create_user.sql** will create a "*main*" database and a "*mysql*" database user with all permissions
* **populate.sh** will drop all tables(if they already exists) recreate them and populate them with the data in *struct/*
* **console.sh** will log you in as "*mysql*" on localhost and open a mysql console for the "*main*" database

## Encountered Issues

* Datagrip on ubuntu has serious performance problems when importing dump files (csv, json, sql inserts)
* Default ubuntu mysql package has major version 5 which does not include the *WITH* clause
* Database contains unicode characters in columns which should be unique and they were not handled properly
* Both bash and Datagrip did not properly display unicode characters correctly
* Serious performance issues when reimporting the dataset into schema with constraints via Datagrip
* MySQL has by default the "ONLY_FULL_GROUP_BY" enabled which places quite prohibitive restrictions on the arguments of *GROUP BY*
* Countless MySQL arbitrary specifics, restrictions and security policies

## Notes

Provided data has fairly inconsistent relationships, some assumptions were made based on the required queries(Lab 1, 2, 3). All queries were written to run in an acceptable time on my machine.


