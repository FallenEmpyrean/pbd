# customer is eligible for rental

delimiter //
create TRIGGER customer_is_eligible
    BEFORE insert
    ON rental
    FOR EACH ROW
BEGIN
    IF due_books(new.id_customer) > 0 and
       (select end_of_subscription from customer where customer.id = new.id_customer) > now()
    THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'cannot create a rental for a customer with due books';
    END IF;
END;
//
delimiter ;

# same book copy cannot be transferred directly between customers

delimiter //
create TRIGGER no_direct_transfers
    BEFORE insert
    ON rental
    FOR EACH ROW
BEGIN
    IF count((select * from rental where id_book_copy = new.id_book_copy and returned = false))
    THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'costumers may not exchange books';
    END IF;
END;
//
delimiter ;

# book cannot be deleted while a copy is rented

delimiter //
create TRIGGER no_deletion_rented_copies
    BEFORE delete
    ON book
    FOR EACH ROW
BEGIN
    IF count(
        (select * from rental
    left join book_copy bc on rental.id_book_copy = bc.id
            where  bc.id= old.id and returned = false))
    THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'book cannot be deleted while a copy is rented';
    END IF;
END;
//
delimiter ;

# may not extend rental period

delimiter //
create TRIGGER no_extensions
    BEFORE update
    ON rental
    FOR EACH ROW
BEGIN
    IF old.rent_date != new.rent_date or old.due_date != new.due_date
    THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'rental period may no be extended without bringing book for inspection';
    END IF;
END;
//
delimiter ;
