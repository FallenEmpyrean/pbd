# customer history

DELIMITER //
DROP PROCEDURE IF EXISTS customer_history //
CREATE PROCEDURE
    customer_history(customer_name VARCHAR(50))
BEGIN
    select b.title, a.name, rent_date, returned
    from rental
             left join customer on rental.id_customer = customer.id
             left join book_copy bc on rental.id_book_copy = bc.id
             left join book b on bc.id_book = b.id
             left join author a on b.id_author = a.id
    where customer.name = customer_name;
END
//

# book stocks

DELIMITER //
DROP PROCEDURE IF EXISTS book_stock //
CREATE PROCEDURE
    book_stock(book_id int(11))
BEGIN
    select received_date,
           edition,
           issuer,
           is_hardback,
           (select count(*) from rental where rental.id_book_copy = book_copy.id and returned = false)
               > 0 as `borrowed`
    from book_copy
             left join book on book_copy.id_book = book.id
    group by book_copy.id;
END
//

# create rental

DELIMITER //
DROP PROCEDURE IF EXISTS create_rental //
CREATE PROCEDURE
    create_rental(_id_book_copy int(11),
                  _id_customer int(11),
                  _rent_date datetime,
                  _id_employee int(11))
BEGIN
    insert into rental (id_book_copy, id_customer, id_employee, rent_date, due_date, returned)
    VALUES (_id_book_copy, _id_customer, _id_employee, rent_date, date_add(_rent_date, interval 1 MONTH), false);
END
//

# create rental

DELIMITER //
DROP PROCEDURE IF EXISTS finish_rental //
CREATE PROCEDURE
    finish_rental(_rental_id int(11))
BEGIN
    update rental set returned = true
    where id = _rental_id;
END
//

