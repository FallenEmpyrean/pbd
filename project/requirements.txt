Tema (mini-proiect):
Construiți o bază de date, corespunzătoare unui domeniu de interes (ex. comerț electronic, gestiune hotelieră,
cabinet medical) care să conțină 8-10 tabele, precum şi următoarele elemente:

 Diagrama bazei de date (relații 1-n, m-n, relații recursive)
 Constrângeri (de unicitate, de domeniu, de integritate referențială)
 Vederi (cel puțin 4)
 Funcții Transact-SQL (cel puțin 2)
 Proceduri stocate (4 sau 5) – de inserare, actualizare, ştergere, căutare, vizualizare
(cu parametri)
 Triggere (cel puțin 4) - inserare, actualizare, ştergere
 Rapoarte (cel puțin 4) – cu funcții de agregare sau Windows Functions, cu
parametri, cu imagini şi/sau grafice, cu subrapoarte

Tema trebuie însoțită de o documentație care să descrie domeniul proiectului,
structura bazei de date (diagrama), precum şi celelalte elemente implementate în baza de date (format .docx sau .doc).

Deadline final mini-proiect:
Folder-ul cu tot proiectul plus documentatia aferenta acestuia se va arhiva (Nume_Prenume.zip) si se va trimite pana in
27 Mai 2020 la ora 24 pe adresa de e-mail: laborator.pbd@gmail.com
(Rugaminte: atunci cand trimiteti mail va rog sa scrieti la subiect:
Proiect final PBD )