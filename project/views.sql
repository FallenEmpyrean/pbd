# book copies on stock and where they can be found

drop view if exists book_copies_on_stock;
create view book_copies_on_stock as
select title, a.name, se.name, se.number, sh.number
from book_copy
         left join book b on book_copy.id_book = b.id
         left join author a on b.id_author = a.id
         left join shelf sh on book_copy.id_shelf = sh.id
         left join section se on sh.id_section = se.id
where book_copy.id not in (
    select id_book_copy
    from rental
    where due_date > now()
);

# in what sections each author can be found

drop view if exists sections_per_author;
create view sections_per_author as
select a.name, group_concat(concat(s2.name, ' '))
from book_copy
         left join shelf s on book_copy.id_shelf = s.id
         left join section s2 on s.id_section = s2.id
         left join book b on book_copy.id_book = b.id
         left join author a on b.id_author = a.id
group by a.id;

# customers which are over due date

drop view if exists overdue_customers;
create view overdue_customers as
select c.name, min(due_date) as `oldest due date`, count(*)
from rental
         left join customer c on rental.id_customer = c.id
where due_date < now() and returned = false;

# special edition books

drop view if exists special_edition_books;
create view special_edition_books as
select b.title, edition, a.name
from book_copy
left join book b on book_copy.id_book = b.id
left join author a on b.id_author = a.id
where edition is not null;