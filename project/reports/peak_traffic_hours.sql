select hour(rent_date), count(*) as 'total rentals' from rental
group by hour(rent_date)
order by `total rentals`;