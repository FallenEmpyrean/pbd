select genre.name, count(*) as `number of books` from genre
left join book_genre bg on genre.id = bg.genre_id
left join book b on bg.book_id = b.id
group by genre.id
order by `number of books`;