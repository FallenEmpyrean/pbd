select b.title, a.name,  count(*) AS `Count`  from rental
left join book_copy bc on rental.id_book_copy = bc.id
left join book b on bc.id_book = b.id
left join author a on b.id_author = a.id
group by b.id
order by Count;