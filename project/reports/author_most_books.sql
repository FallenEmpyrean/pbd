select a.name, count(*) as `book #` from book
left join author a on book.id_author = a.id
group by a.id
order by `book #`;