SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `library`;
CREATE SCHEMA IF NOT EXISTS `library` DEFAULT CHARACTER SET utf8;
USE `library`;

-- -----------------------------------------------------
-- Table `library`.`section`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`section`
(
    `id`     INT(11)     NOT NULL AUTO_INCREMENT,
    `name`   VARCHAR(50) NULL DEFAULT NULL,
    `number` INT(11)     NOT NULL,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `library`.`shelf`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`shelf`
(
    `id`         INT(11) NOT NULL AUTO_INCREMENT,
    `number`     INT(11) NOT NULL,
    `id_section` INT(11) NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_shelf_section`
        FOREIGN KEY (`id_section`)
            REFERENCES `library`.`section` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table `library`.`author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`author`
(
    `id`    INT(11)     NOT NULL AUTO_INCREMENT,
    `name`  VARCHAR(50) NULL DEFAULT NULL,
    `birth` DATETIME    NOT NULL,
    `death` DATETIME    NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `library`.`book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`book`
(
    `id`        INT(11)      NOT NULL AUTO_INCREMENT,
    `title`     VARCHAR(100) NOT NULL,
    `year`      DATE         NOT NULL,
    `id_author` INT(11)      NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_book_author`
        FOREIGN KEY (`id_author`)
            REFERENCES `library`.`author` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table `library`.`genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`genre`
(
    `id`   INT(11)     NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `library`.`book_genre`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`book_genre`
(
    `book_id`  INT(11) NOT NULL,
    `genre_id` INT(11) NOT NULL,
    PRIMARY KEY (`book_id`, `genre_id`),
    CONSTRAINT `fk_book_genre_book`
        FOREIGN KEY (`book_id`)
            REFERENCES `library`.`book` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `fk_book_genre_genre`
        FOREIGN KEY (`genre_id`)
            REFERENCES `library`.`genre` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table `library`.`book_copy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`book_copy`
(
    `id`            INT(11)     NOT NULL AUTO_INCREMENT,
    `id_shelf`      INT(11)     NOT NULL,
    `id_book`       INT(11)     NOT NULL,
    `received_date` DATETIME    NOT NULL,
    `edition`       VARCHAR(50) NULL     DEFAULT NULL,
    `issuer`        VARCHAR(50) NULL     DEFAULT NULL,
    `is_hardback`   BOOL        NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_book_copy_shelf`
        FOREIGN KEY (`id_shelf`)
            REFERENCES `library`.`shelf` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,
    CONSTRAINT `fk_book_copy_book`
        FOREIGN KEY (`id_book`)
            REFERENCES `library`.`book` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table `library`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`customer`
(
    `id`                  INT(11)      NOT NULL AUTO_INCREMENT,
    `name`                VARCHAR(50)  NULL DEFAULT NULL,
    `address`             VARCHAR(150) NOT NULL,
    `end_of_subscription` DATE         NOT NULL,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `library`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`employee`
(
    `id`   INT(11)     NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

-- -----------------------------------------------------
-- Table `library`.`rental`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`rental`
(
    `id`           INT(11)  NOT NULL AUTO_INCREMENT,
    `id_book_copy` INT(11)  NOT NULL,
    `id_customer`  INT(11)  NOT NULL,
    `id_employee`  INT(11)  NOT NULL,
    `rent_date`    DATETIME NOT NULL,
    `due_date`     DATETIME NOT NULL,
    `returned`     bool     NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_rental_book_copy`
        FOREIGN KEY (`id_book_copy`)
            REFERENCES `library`.`book_copy` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,
    CONSTRAINT `fk_rental_employee`
        FOREIGN KEY (`id_employee`)
            REFERENCES `library`.`employee` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,
    CONSTRAINT `fk_rental_customer`
        FOREIGN KEY (`id_customer`)
            REFERENCES `library`.`customer` (`id`)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
