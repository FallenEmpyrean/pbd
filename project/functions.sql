# customer's number of due books
create function due_books(customer_id int(11)) returns integer deterministic
    return (select count(*)
            from rental
            where id_customer = customer_id and due_date < now() and returned = false);

# number of copies of selected book in stock
create function in_stock(book_id int(11)) returns integer deterministic
    return (select count(*)
            from book_copy
                     left join rental r on book_copy.id = r.id_book_copy
                     left join book b on book_copy.id_book = b.id
            where b.id = book_id);